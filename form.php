<?php


get_header();
?>
<h3 class="text-center form-title">Preencha o nosso formulário!</h3>
<form  class="container" id="formulario">
    <div class="row">
        <div class="col-lg-6">

        <div class="form-group">
            <input type="text" class="form-control" id="nome"  placeholder="Seu nome" required>
            <div class="feedback"></div>
            <input type="email" class="form-control" id="email"  placeholder="Seu e-mail" required>
            <input type="text" class="form-control" id="tel"  placeholder="Seu telefone" required>
            <input type="text" class="form-control" id="nascimento"  placeholder="Data de Nascimento" required>
        </div>

        </div>

        <div class="col-lg-6">
            
        <div class="form-group">
            <input type="text" class="form-control" id="cep"  placeholder="CEP" required>
            <div class="row">
                <div class="col-8"><input type="text" class="form-control" id="endereco"  placeholder="Endereço" required></div>
                <div class="col-4"><input type="text" class="form-control" id="numero"  placeholder="Número" required></div>
            </div>
            <input type="text" class="form-control" id="bairro"  placeholder="Bairro" required>
            <div class="row">
                <div class="col-8"><input type="text" class="form-control" id="cidade"  placeholder="Cidade" required></div>
                <div class="col-4"><input type="text" class="form-control" id="estado"  placeholder="Estado" required></div>
            </div>
        </div>

        </div>
    </div>

    <button type="submit" id="envia" class="btn btn-lg mx-auto d-block">Enviar Dados</button>
</form>