var noticias = {

init: function(){

    noticias.cors();
    noticias.dados();
  
},
cors: function(){
    var ec2 = new AWS.EC2({region: 'us-east-1c'});

    ec2.describeInstances(function(err, data) {
      if (err) {
        console.log(err);
      } else {
        data.Reservations.forEach(function(reservation) {
          reservation.Instances.forEach(function(instance) {
            console.log(instance.InstanceId);
          });
        });
      }
    });
},

dados: function () {
    
    $.ajax({
        url: 'https://brasil.uxdesign.cc/feed',
        crossDomain: true,
        dataType:'xml',
        success: function(data){

            noticias.cards(data.activeElement.lastElementChild)
            
        }
    })
},

cards: function(obj){

    var wrap = $("#noticias"),
        canal = $.parseXML(obj.outerHTML),
        item = $(canal).find("item");

        $.each($(item), function(k, v){

             wrap.append(
            '<div class="card col-xl-4">' +

                '<div class="card-img-top" style=" background-image: url('+ $(v.outerHTML).find("img")[0].currentSrc +');">' +

                '<span class="badge">'+ $(v).find('category')[0].textContent + '</span></div>' +
                
                '<div class="card-body">' +

                    '<h5 class="card-title">' + $(v).find("title").text().slice(0,50) + '...</h5>' +

                    '<p class="card-text">'+ $(v.outerHTML).find('p').text().slice(0,139) +'...</p>' +
                    
                    '<a href="'+ $(v).find('link')[0].innerHTML +'" class="btn btn-block btn-outline-dark" target="_blank">Link Externo</a>' +

                '</div>' +
           '</div>')

        })
        
        $( ".card" ).first().addClass('active');

        noticias.carrossel();

},

carrossel: function(){

    $( '#noticias' ).cycle({
        slides: ".card",
        fx: "carousel",
        carouselVisible: 3,
    });

},

}

$(document).ready( function(){

    noticias.init();

})