$(document).ready(function(){

    $('#nome, #endereco, #bairro, #cidade, #estado').mask(
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAA' +
        'AAAAAAAAAAA', 
        {'translation': {
        A: {pattern: /\D/gm} 
      } 
  });

    var maskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      options = {onKeyPress: function(val, e, field, options) {
              field.mask(maskBehavior.apply({}, arguments), options);
          }
      };
      
      $('#tel').mask(maskBehavior, options);
      $('#nascimento').mask('00/00/0000');
      $('#cep').mask('00000-000');
      $('#numero').mask('0#')

})