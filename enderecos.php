<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4>Atendimento Brasil <img src=<?php echo get_theme_file_uri("assets/images/brasil.png")?> /></h4>
            <hr>
            <address>Av. Lineu de Paula Machado, nº 1477 - Jardim Everest - São Paulo - SP - CEP: 05601-001</address>
        </div>
        <div class="col-md-4">
            <h4>Produção Brasil <img src=<?php echo get_theme_file_uri("assets/images/brasil.png")?> /></h4>
            <hr>
            <address>Rua Com. Araújo, nº 499 - Curitiba - PR - CEP: 80420-000</address>
        </div>
        <div class="col-md-4">
            <h4>Atendimento EUA <img src=<?php echo get_theme_file_uri("assets/images/eua.png")?> /></h4>
            <hr>
            <address>1615 South Congress Avenue, Delray Beach - FL</address>
        </div>
    </div>
</div>