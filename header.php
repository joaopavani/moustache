<?php header("Access-Control-Allow-Origin: *"); ?>

<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://sdk.amazonaws.com/js/aws-sdk-2.1.34.min.js"></script>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/cycle2.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/cycle2-carousel.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/home.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/noticias.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/mascaras.js')?>"></script>
		<script src="<?php echo get_theme_file_uri('assets/js/valida.js')?>"></script>
		<link rel="stylesheet/less" href="<?php echo get_theme_file_uri('assets/css/_cards.less') ?>">
		<link rel="stylesheet/less" href="<?php echo get_theme_file_uri('assets/css/main.less') ?>">

		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js" ></script>
		

	</head>

	<body <?php body_class(); ?>>

		
		<nav>

		<div class="container nav">
	
		<div class="col-9">
		<li class="page_item navbar-brand"><img src=<?php echo get_theme_file_uri("assets/images/bigode.png")?> alt="Moustache"></li>
		<?php
								if ( has_nav_menu( 'primary' ) ) {

									wp_nav_menu(
										array(
											'container'  => '',
											'items_wrap' => '%3$s',
											'theme_location' => 'primary',
										)
									);

								} elseif ( ! has_nav_menu( 'expanded' ) ) {

									wp_list_pages(
										array(
											'match_menu_classes' => true,
											'show_sub_menu_icons' => true,
											'title_li' => false,
										)
									);

								}

								
								?>
								</div>
		<div class="col-3">						
		<?php get_search_form(); ?>
		</div>
		</div>
		</nav>

		<?php

