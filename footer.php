<?php


?>

			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner row">

					<div class="footer-credits col-9">

						<p class="footer-copyright">Copyright &copy;
							<?php
							echo date_i18n(
								/* translators: Copyright date format, see https://secure.php.net/date */
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
						Agência Moustache LTDA.	
						</p><!-- .footer-copyright -->

						<p class="powered-by-wordpress">
							Todos os direitos reservados
						</p><!-- .powered-by-wordpress -->

					</div><!-- .footer-credits -->
				
					<div class="col-3 links">
					<a class="a" href="#">
						Link Externo 1  
					</a>
							<span> | </span>
					<a class="a" href="#">
						Link Externo 2
					</a>
					</div>

				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
